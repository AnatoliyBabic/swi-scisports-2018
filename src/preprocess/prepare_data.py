import os
import random
from pprint import pprint
from time import time

import pandas as pd
import numpy as np

from src.visual.plot_stuff import plot_dynamic, plot_static


class Matches:
    def __init__(self):
        self.match_dict = Match.make_all_matches()

    def next_batch_from_all(self, size, length=100, player_id=0, start=None, aggregate=1, centered=False):
        if player_id == 0:
            match_ids = list(self.match_dict.keys())
        else:
            match_ids = []
            for match_id in self.match_dict.keys():
                if player_id in self.match_dict[match_id].players:
                    match_ids.append(match_id)

        return np.array([self.match_dict[random.choice(match_ids)].sample_trajectory(length=length, player_id=player_id,
                                                                                     start=start, aggregate=aggregate,
                                                                                     centered=centered)
                         for i in range(size)])

    def next_batch_ball_from_all(self, size, length=100, player_id=0, start=None, aggregate=1, centered=False):
        if player_id == 0:
            match_ids = list(self.match_dict.keys())
        else:
            match_ids = []
            for match_id in self.match_dict.keys():
                if player_id in self.match_dict[match_id].players:
                    match_ids.append(match_id)

        return np.array([self.match_dict[random.choice(match_ids)].ball_trajectory(length=length, start=start,
                                                                              aggregate=aggregate, centered=centered)
                         for i in range(size)])

    def next_batch_player_ball_from_all(self, size, length=100, player_id=0, start=None, aggregate=1, centered=False):
        if player_id == 0:
            match_ids = list(self.match_dict.keys())
        else:
            match_ids = []
            for match_id in self.match_dict.keys():
                if player_id in self.match_dict[match_id].players:
                    match_ids.append(match_id)

        return np.array([self.match_dict[random.choice(match_ids)].player_and_ball(length=length, player_id=player_id,
                                                                              start=start, aggregate=aggregate)
                         for i in range(size)])


class Match:
    def __init__(self, match_id):
        self.data = None
        self.match_id = match_id
        self.players = []
        self.players_of_team = {}
        self.role_of_player = {}
        self.mirror_half = 0
        self.field_length = 0
        self.field_width = 0

        self.match_trajectories = {}

    @staticmethod
    def make_all_matches():
        match_ids = [77264, 77265, 77317, 77816, 78344, 78438, 80568, 81594, 82326, 82468, 82476, 83244]
        match_dict = {}
        for match_id in match_ids:
            match_dict[match_id] = Match(match_id)
            match_dict[match_id].build()
        return match_dict

    def plot_match(self, half=1, t_start=0, t_duration=100):
        T1, T2 = t_start*1000, (t_start+t_duration)*1000
        teams = list(self.players_of_team.keys())
        teams = list(reversed(teams)) if teams[0] != 32311 else teams
        players = [player for player in self.players_of_team[teams[0]]] + \
                  [player for player in self.players_of_team[teams[1]]] + [-1]
        colours = ['red' for i in range(len(self.players_of_team[teams[0]]))] + \
                  ['blue' for i in range(len(self.players_of_team[teams[1]]))] + ['gray']
        trajectories = np.zeros((len(players), t_duration*10, 2))

        for i, player in enumerate(players):
            traj = self.match_trajectories[player]
            if len(traj.loc[(T1 <= traj["T"]) & (traj["T"] < T2) & (traj["IdHalf"] == half)][["T", "X", "Y"]]) < t_duration*10:
                # print(player, "NaN")
                k = 0
                for t in range(T1, T2, 100):
                    a = traj.loc[(traj["T"] == t) & (traj["IdHalf"] == half)][["T", "X", "Y"]]
                    trajectories[i][k] = [float("NaN"), float("NaN")] if len(a.values)==0 else [a.values[0][1], a.values[0][2]]
                    k += 1
            else:
                # print(player, "ok")
                trajectories[i] =\
                    np.array(traj.loc[(T1 <= traj["T"]) & (traj["T"] < T2) & (traj["IdHalf"] == half)][["X", "Y"]])
        return trajectories, colours

    def build(self):
        self.read_data()
        self.build_player_data()
        self.build_match_trajectories()

        # field sizes from 'field_sizes.csv'
        if self.match_id in [77264, 77265, 77317, 77816, 78344, 78438, 80568, 81594, 82326, 82468, 82476, 83244]:
            self.field_length, self.field_width = 10500, 6800
        elif self.match_id == 82841:
            self.field_length, self.field_width = 10200, 6700
        else:
            pass

    def read_data(self):
        data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..\\..\\data")
        for filename in os.listdir(data_dir):
            if filename.startswith("xydata") and str(self.match_id) in filename:
                self.data = pd.read_csv(os.path.join(data_dir, filename))
                self.data = self.data[['T', 'X', 'Y', 'player_id', 'match_id', 'IdHalf']]
                break
        else:  # if did not find an xydata file with 'match_id' in the filename
            print("Match file not found!")
            return

    def build_player_data(self):
        data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..\\..\\data")
        for filename in os.listdir(data_dir):
            if filename.startswith("players") and str(self.match_id) in filename:
                players_data = pd.read_csv(os.path.join(data_dir, filename))
                players_data = players_data[['IdActor', 'IdTeam', 'Occupation', 'Position']]
                break
        else:  # if did not find a players file with 'match_id' in the filename
            print("Players' file not found!")
            return

        self.players = list(players_data.loc[players_data["Occupation"]=="Player"]["IdActor"].unique())
        for team_id in players_data["IdTeam"].unique():
            self.players_of_team[int(team_id)] = list(players_data.loc[(players_data["Occupation"]=="Player") & (players_data["IdTeam"]==team_id)]["IdActor"].unique())
        for player_id in self.players:
            self.role_of_player[player_id] = players_data.loc[players_data["IdActor"]==player_id]["Position"].values[0]
        # team_gk = players_data[(players_data["IdTeam"]==32311) & (players_data["Position"]=="Goalkeeper")]["IdActor"].values[0]

    # builds the player trajectories ([x,y] for t=1..T)
    # output: dictionary, where keys are (int) player_id's
    def build_match_trajectories(self):
        trajectory_dict = {}
        player_ids = self.data["player_id"].unique()
        for player_id in player_ids:
            unsorted_traj = self.data.loc[self.data["player_id"] == player_id]
            trajectory_dict[int(player_id)] = \
                unsorted_traj.sort_values(["IdHalf", "T"])[["IdHalf", "T", "X", "Y"]]
        self.match_trajectories = trajectory_dict

        # the next part makes sure that our team (32311) will always be on
        # the left side of the field, attacking the right side of the field
        team_gk = 390565
        traj = trajectory_dict[team_gk]
        if traj.loc[traj["IdHalf"]==1]["X"].mean() < 0 < traj.loc[traj["IdHalf"]==2]["X"].mean():
            self.mirror_half = 2
        elif traj.loc[traj["IdHalf"]==2]["X"].mean() < 0 < traj.loc[traj["IdHalf"]==1]["X"].mean():
            self.mirror_half = 1
        else:
            print("Weird goalkeeper, check data!")

        for player_id in player_ids:
            traj_tmp = trajectory_dict[player_id]
            traj_tmp.loc[traj_tmp["IdHalf"]==self.mirror_half, ["X", "Y"]] = \
                traj_tmp.loc[traj_tmp["IdHalf"]==self.mirror_half, ["X", "Y"]]*-1
            trajectory_dict[player_id] = traj_tmp

    def player_and_ball(self, length=100, player_id=0, start=None, aggregate=1):
        pick_length = aggregate * length
        if player_id == 0:
            player_id = random.choice(
                [player for player in self.players if self.role_of_player[player] != "Goalkeeper"])
        traj = self.match_trajectories[player_id]
        traj_ball = self.match_trajectories[-1]  # the ball
        possible_halves = []
        if len(traj.loc[traj["IdHalf"] == 1]) > 0:
            possible_halves.append(1)
        if len(traj.loc[traj["IdHalf"] == 2]) > 0:
            possible_halves.append(2)
        if not possible_halves:
            print("player", player_id, "does not play")
            return None
        # print(self.match_id, player_id)
        # debug mode: fixed half and start for every function call (not player)
        if start:
            half = possible_halves[0]
            traj_h = traj.loc[traj["IdHalf"] == half]
            traj_ball_h = traj_ball.loc[traj_ball["IdHalf"] == half]
            start_t = start
        else:
            half = random.choice(possible_halves)
            traj_h = traj.loc[traj["IdHalf"] == half]
            traj_ball_h = traj_ball.loc[traj_ball["IdHalf"] == half]
            start_t = random.randint(0, min(len(traj_h)-pick_length, len(traj_ball_h)-pick_length))

        #print(len(traj_h), len(traj_ball_h), self.match_id, start_t)
        if aggregate == 1:
            sample = np.array(traj_h.iloc[range(start_t, start_t + length)][["X", "Y"]])
            ball = np.array(traj_ball_h.iloc[range(start_t, start_t + length)][["X", "Y"]])
        else:
            sample = np.array(traj_h.iloc[range(start_t, start_t + pick_length)][["X", "Y"]])\
                .reshape(length, aggregate, -1).mean(axis=1)
            ball = np.array(traj_ball_h.iloc[range(start_t, start_t + pick_length)][["X", "Y"]])\
                .reshape(length, aggregate, -1).mean(axis=1)

        return np.array([sample, ball])

    def ball_trajectory(self, length=100, start=None, aggregate=1, centered=False):
        #print(self.match_id)
        pick_length = aggregate * length
        traj = self.match_trajectories[-1]

        # debug mode: fixed half and start for every function call
        if start:
            half = 1
            traj_h = traj.loc[traj["IdHalf"] == half]
            size_traj_h = len(traj_h)
            start_t = start
        else:
            half = random.choice([1, 2])
            traj_h = traj.loc[traj["IdHalf"] == half]
            size_traj_h = len(traj_h)
            start_t = random.randint(0, size_traj_h - pick_length)

        if aggregate == 1:
            sample = np.array(traj_h.iloc[range(start_t, start_t + length)][["X", "Y"]])
        else:
            sample = np.array(traj_h.iloc[range(start_t, start_t + pick_length)][["X", "Y"]])\
                .reshape(length, aggregate, -1).mean(axis=1)

        if centered:
            return sample - sample[0]
        else:
            return sample

    # gives a random trajectory with length 'length' from the specified player
    # if no 'player_id' is given, it randomly choses one
    # 'length' is always the length of the trajectory returned
    #   it just samples from a trajectory with length 'aggregate*length'
    # 'centered': moving each trajectory such that they start at (0,0)
    def sample_trajectory(self, length=100, player_id=0, start=None, aggregate=1, centered=False):
        pick_length = aggregate*length
        if player_id == 0:
            player_id = random.choice(
                [player for player in self.players if self.role_of_player[player] != "Goalkeeper"])
        traj = self.match_trajectories[player_id]
        possible_halves = []
        if len(traj.loc[traj["IdHalf"] == 1]) > 0:
            possible_halves.append(1)
        if len(traj.loc[traj["IdHalf"] == 2]) > 0:
            possible_halves.append(2)
        if not possible_halves:
            print("player", player_id, "does not play")
            return None
        # print(self.match_id, player_id)
        # debug mode: fixed half and start for every function call (not player)
        if start:
            half = possible_halves[0]
            traj_h = traj.loc[traj["IdHalf"] == half]
            size_traj_h = len(traj_h)
            start_t = start
        else:
            half = random.choice(possible_halves)
            traj_h = traj.loc[traj["IdHalf"] == half]
            size_traj_h = len(traj_h)
            start_t = random.randint(0, size_traj_h-pick_length)

        if aggregate == 1:
            sample = np.array(traj_h.iloc[range(start_t, start_t + length)][["X", "Y"]])
        else:
            sample = np.array(traj_h.iloc[range(start_t, start_t + pick_length)][["X", "Y"]])\
                .reshape(length, aggregate, -1).mean(axis=1)
        if centered:
            return sample-sample[0]
        else:
            return sample

    # gives 'size' many trajectories with length 'length' for player 'player_id'
    def next_batch(self, size, length=100, player_id=0, start=None, aggregate=1, centered=False):
        return np.array([self.sample_trajectory(length=length, player_id=player_id,
                                                start=start, aggregate=aggregate, centered=centered)
                         for i in range(size)])

    def next_batch_ball(self, size, length=100, start=None, aggregate=1, centered=False):
        return np.array([self.ball_trajectory(length=length, start=start,
                                                aggregate=aggregate, centered=centered)
                         for i in range(size)])

    def next_batch_player_ball(self, size, length=100, start=None, aggregate=1):
        return np.array([self.player_and_ball(length=length, start=start, aggregate=aggregate) for i in range(size)])


    def check_teleport(self, player_id):
        traj = self.match_trajectories[player_id]
        if len(traj.loc[(abs(traj["X"]) > self.field_length) | (abs(traj["Y"]) > self.field_width)]) > 0:
            print("match ", self.match_id, ", player ", player_id)
            print(traj.loc[(abs(traj["X"]) > self.field_length) | (abs(traj["Y"]) > self.field_width)])

if __name__ == "__main__":
    pass
