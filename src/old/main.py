# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 11:12:36 2018

@author: Anatoliy
"""
import pandas as pd
import numpy as np

import old.load_data as load_data
from old.calculate_velocity import calculate_velocity

if __name__ == '__main__':
    print('Loading data')
    positional_data = load_data.load_all_xydata('.\data')
    print('Calculating velocity')
    positional_data = {key:calculate_velocity(value) for key,value in positional_data.items()}
    
    print('Aggregating the data')
    all_data_df = pd.concat(positional_data.values())
    
    print('Selecting match to animate')
    single_match_id = np.choose([1],all_data_df.index.get_level_values('match_id').unique())[0]
    single_match_data = all_data_df[all_data_df.index.get_level_values('match_id') == single_match_id]
    single_half_data = single_match_data[single_match_data.index.get_level_values('IdHalf') ==1]
    
    my_animation = animation.AnimatedScatter(data= single_half_data)
    my_animation.show()