# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 10:29:54 2018

@author: Anatoliy
"""

import pandas as pd
import os

DATA_TYPES = ['events','player_info','xydata']

def load_match_xydata(file):
    data = pd.read_csv(file)
    data = data[['T','X','Y','player_id','match_id', 'IdHalf']]
    data.set_index(['player_id', 'match_id', 'IdHalf','T'],inplace=True)
    data = data.sort_index()
    return data

def load_all_xydata(DATA_DIR):
    DATA_FILES = [i for i in os.listdir(DATA_DIR) if 'xydata' in i]

    all_data = {}
    for file in DATA_FILES:
        match_id = file.split('_')[1]
        filename = os.path.join(DATA_DIR,file)
        print(file)
        all_data[match_id] = load_match_xydata(filename)
    return all_data

def load_match_players(file):
    return pd.read_csv(file)

def load_all_players(DATA_DIR):
    DATA_FILES = [i for i in os.listdir(DATA_DIR) if 'players' in i]

    all_data = {}
    for file in [i for i in DATA_FILES]:
        match_id = file.split('_')[1]
        filename = os.path.join(DATA_DIR,file)
        all_data[match_id] = load_match_players(filename)
    return all_data

def load_match_events(file):
    return pd.read_csv(file)

def load_all_events(DATA_DIR):
    DATA_FILES = [i for i in os.listdir(DATA_DIR) if 'events' in i]

    all_data = {}
    for file in [i for i in DATA_FILES]:
        match_id = file.split('_')[1]
        filename = os.path.join(DATA_DIR,file)
        all_data[match_id] = load_match_players(filename)
    return all_data

if __name__ == '__main__':
    DATA_DIR = '..\..\data'

    #all_player_data= load_all_players(DATA_DIR)
    #all_event_data = load_all_events(DATA_DIR)
    all_xydata_data = load_all_xydata(DATA_DIR)
