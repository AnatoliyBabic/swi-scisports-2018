# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 10:48:35 2018

@author: Anatoliy
"""

import pandas as pd
import numpy as np

def calculate_velocity(match_df):
    players = match_df.index.get_level_values('player_id').unique()
    match_df['velocity_x'], match_df['velocity_y'], match_df['velocity'] = np.nan, np.nan, np.nan
    for player in players:
        player_data = match_df[match_df.index.get_level_values('player_id') == player].copy()

        player_data['velocity_x'] = player_data['X'].diff()
        player_data['velocity_y'] = player_data['Y'].diff()
        player_data['velocity'] = np.sqrt(player_data['velocity_x']**2 + player_data['velocity_y']**2)

        match_df[match_df.index.get_level_values('player_id') == player] = player_data
    return match_df


def calculate_displacement(match_df,timeframes):
    players = match_df.index.get_level_values('player_id').unique()
    match_df['delta_'+str(timeframes)], match_df['delta_y_'+str(timeframes)], match_df['distance_'+str(timeframes)] = np.nan, np.nan, np.nan
    for player in players:
        player_data = match_df[match_df.index.get_level_values('player_id') == player].copy()

        player_data['velocity_x'] = player_data['X'].diff()
        player_data['velocity_y'] = player_data['Y'].diff()
        player_data['velocity'] = np.sqrt(player_data['velocity_x']**2 + player_data['velocity_y']**2)

        match_df[match_df.index.get_level_values('player_id') == player] = player_data
    return match_df