import os
import pickle
from pprint import pprint
from time import time

import numpy as np

from preprocess.prepare_data import Match, Matches
from visual.plot_stuff import plot_static, plot_dynamic

if False:
    m = Matches()
    for i in range(10):
        a = m.next_batch_player_ball_from_all(10000, length=200)
        print(i+1)


if True:
    m = Match(77317)
    m.build()
    data_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..\\..\\data")
    arr1 = pickle.load(open(os.path.join(data_folder, "player342459_true.p"), "rb"))
    arr2 = pickle.load(open(os.path.join(data_folder, "player342459_sim.p"), "rb"))
    print(arr1)
    print(arr2)
    arr = np.array([arr1[0], arr2[0]])
    plot_static(arr, 10500, 6800, colours=["red", "blue"],
                filename=os.path.join(data_folder, "anuj1.png"))
    plot_dynamic(arr, 10500, 6800, colours=["red", "blue"], mspf=100,
                 filename=os.path.join(data_folder, "anuj1.mp4"))

if False:
    data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..\\..\\data")
    player_dict = {}
    for filename in os.listdir(data_dir):
        if filename.startswith("players"):
            ifile = open(os.path.join(data_dir, filename), "r")
            for line in ifile:
                sline = line.strip().split(",")
                if len(sline[1]) == 6:
                    if sline[1] not in player_dict:
                        player_dict[sline[1]] = 0
                    player_dict[sline[1]] += 1
    a1_sorted_keys = sorted(player_dict, key=player_dict.get, reverse=True)
    for r in a1_sorted_keys:
        print(r, player_dict[r])

if False:
    m = Match(77317)
    m.build()
    # a = m.next_batch(size=1, length=100, player_id=343639, aggregate=1, centered=True, debug=True)
    # print(a)
    #trajs, clrs = m.plot_match(half=1, t_start=0, t_duration=30)
    #plot_static(trajs, m.field_length, m.field_width, colours=clrs).show()
    a = m.next_batch(10, length=100, player_id=385047)
    pprint(a)
    plot_static(a, m.field_length, m.field_width).show()

if False:
    match_ids = [72951, 77264, 77265, 77317, 77816, 78344, 78438, 80568, 81594, 82326, 82468, 82476, 82841, 83244]
    m = Match(77317)
    m.build()
    # plot_static(np.array(m.match_trajectories[342459].loc[m.match_trajectories[342459]["IdHalf"]==1][["X", "Y"]]), m.field_length, m.field_width).show()

    t = time()
    a = m.next_batch(size=1, length=30, player_id=0, aggregate=1, centered=True)
    print(time() - t)

    t = time()
    b = m.next_batch(size=10, length=30, player_id=0, aggregate=3, centered=False)
    print(time() - t)

    plot_static(b, m.field_length, m.field_width).show()
    plot_dynamic(b, m.field_length, m.field_width).show()


